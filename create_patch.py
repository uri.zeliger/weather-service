import os, sys, re
from git import Repo

class GitLabRepo:
    def __init__(self) -> None:
        print(os. getcwd())
        self.repo = Repo(os. getcwd())
   
        
    def checkout(self, branch_name):
        self.repo.git.checkout(branch_name)
    
    def _check_is_official_release(self, version):
        is_official_release = False
        pattern = r'^release/v\d{1,3}\.\d{1,3}\.\d{1,3}$'
        if re.match(pattern, version):
            is_official_release = True
        return is_official_release
    
    def get_tags(self):
        tags = sorted(self.repo.tags, key=lambda t: t.commit.committed_datetime)
        return [tag.path.replace(f"{tag._common_path_default}/", "") for tag in tags]
    
    def get_2_official_tags(self):
        tags = [tag for tag  in self.get_tags() if self._check_is_official_release(tag)]
        return [tags[-2], tags[-1]]
    
if __name__ == "__main__":
    repo = GitLabRepo()
    tags = repo.get_2_official_tags()
    origin_tag = tags[1]
    target_tag = sys.argv[1]
    print(len(sys.argv))
    if len(sys.argv) == 3:
        origin_tag = sys.argv[2]
    docker_command = f"docker run -i -v ~/.aws/credentials:/root/.aws/credentials:ro  -v ~/.docker/config.json:/root/.docker/config.json  -v /var/run/docker.sock:/var/run/docker.sock  -v /orcaai/orcaai-storage:/orcaai/orcaai-storage   -v /run/systemd/system:/run/systemd/system  -v /var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket   --network host --privileged --cap-add=ALL registry.gitlab.com/orca_ai/orcaai-ship/create-patch:feature-SEAP-1461-create-patch-validation-and-cleanup make create-patch GIT_ORIGIN_VERSION={origin_tag} GIT_TARGET_VERSION={target_tag}"
    clean_docker_containers = "docker rm -f $(docker ps -q -a)"
    clean_docker_images = "docker rmi -f $(docker images -q)"
    clean_artifacts = "rm -rf /orcaai/orcaai-storage/output"
    with open('dkr_cmd.sh', 'w') as dkr_cmd:
        dkr_cmd.write(f"{docker_command} \n")
        dkr_cmd.write(f"{clean_docker_containers} \n")
        dkr_cmd.write(f"{clean_docker_images} \n")


