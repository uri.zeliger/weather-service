from typing import Any, List
from injector import inject, singleton
from core import MongoClient
from motor.motor_asyncio import AsyncIOMotorDatabase, AsyncIOMotorCollection


@singleton
class AlertDAL:
    @inject
    def __init__(self, mongo_client: MongoClient) -> None:
        _db: AsyncIOMotorDatabase = mongo_client.mongo_client["alert"]
        self._collection: AsyncIOMotorCollection = _db["weather"]

    async def get(self, id: str) -> Any:
        result = await self._collection.find_one({'alert_id': {'$eq': id}})
        return result

    async def insert(self, id: str) -> Any:
        result = await self._collection.insert_one({'alert_id': id})
        return result

    async def update(self, alert, id: str, data: dict):
        if not alert.get("samples"):
            alert["samples"] = []
        
        alert["samples"].append(data)
        await self._collection.replace_one({'_id': alert["_id"]}, {**{'alert_id': id}, 'samples': alert["samples"]})
