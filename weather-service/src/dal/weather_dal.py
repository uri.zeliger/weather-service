from injector import inject, singleton
from core import InfluxClient


@singleton
class WeatherDAL:
    @inject
    def __init__(self, influxdb: InfluxClient):
        self._influxdb = influxdb

    async def insert(self, data: dict) -> bool:
        return await self._influxdb.write_point({'measurement': 'weather', 'fields': data})
