from apscheduler.schedulers.asyncio import AsyncIOScheduler


class JobManager:
    def __init__(self):
        self.scheduler = AsyncIOScheduler()

    def add_interval_job(self, callback, interval: float = 30):
        self.scheduler.add_job(callback, 'interval', seconds=interval)

    def start(self):
        self.scheduler.start()

    def close(self):
        self.scheduler.shutdown()
