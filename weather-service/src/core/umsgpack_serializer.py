import umsgpack
from datetime import datetime

def packb(msg: dict):
    return umsgpack.packb(msg)


def unpackb(msg: bytes):
    return umsgpack.unpackb(msg)