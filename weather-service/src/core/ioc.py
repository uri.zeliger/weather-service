import logging
from injector import Module, provider, singleton
from sanic import Sanic

from .logger import init_logger

from .configuration import Configuration
from .influx_client import InfluxClient
from .redis_client import RedisClient
from .mongo_client import MongoClient
from .rest_executer import RestExecuter
from .job_manager import JobManager
from .rabbit_connection import RabbitConnection

class IOC(Module):
    @provider
    def provide_sanic(self, config_serive: Configuration) -> Sanic:
        return Sanic.get_app(config_serive.app_name)

    @singleton
    @provider
    def provide_redis(self, config_serive: Configuration) -> RedisClient:
        return RedisClient(config_serive.redis_host, config_serive.redis_port)

    @provider
    def provide_rabbit(self, config_serive: Configuration) -> RabbitConnection:
        return RabbitConnection(config_serive.rabbit_connection)

    @singleton
    @provider
    def provide_influx(self, config_serive: Configuration) -> InfluxClient:

        return InfluxClient(config_serive.influx_host,
                            config_serive.influx_port,
                            config_serive.influx_username,
                            config_serive.influx_password,
                            config_serive.influx_db)

    @singleton
    @provider
    def provide_mongo(self, config_serive: Configuration) -> MongoClient:
        return MongoClient(config_serive.mongo_connection)

    @singleton
    @provider
    def provide_logger(self) -> logging.Logger:
        init_logger()
        return logging.getLogger()

    @provider
    def provide_rest_executer(self) -> RestExecuter:
        return RestExecuter()

    @singleton
    @provider
    def provide_job_manager(self) -> JobManager:
        return JobManager()

 

