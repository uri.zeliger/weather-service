from aiohttp_retry import RetryClient, ExponentialRetry
from aiohttp import ClientSession
from requests import get, post, Timeout, ConnectionError
from retry import retry


class RestExecuter:
    def __init__(self) -> None:
        self._default_headers: dict = {
            "Content-Type": "application/json; charset=utf-8"}
        pass

    @retry((Timeout, ConnectionError), tries=5)
    def get(self, url: str, params: dict, headers: dict = None):
        if not headers:
            headers = {}

        result = get(url, params=params,
                     headers={**self._default_headers, **headers})

        result.raise_for_status()
        return result.json()

    async def get_async(self, url: str, params: dict, headers: dict = None):
        if not headers:
            headers = {}

        retry_options = ExponentialRetry(attempts=3)
        async with RetryClient(
            raise_for_status=False, retry_options=retry_options) as retry_client:
            async with retry_client.get(url, params=params, retry_options=retry_options, headers={**self._default_headers, **headers}) as response:
                return await response.json()

    def post(self, url: str, params: dict, body: dict, headers: dict = None):
        if not headers:
            headers = {}

        result = post(url, params=params, json=body, headers={
                      **self._default_headers, **headers})

        result.raise_for_status()
        return result.json()

    async def post_async(self, url: str, params: dict, body: dict, headers: dict = None):
        if not headers:
            headers = {**headers, **self._default_headers}

        async with ClientSession() as session:
            async with session.post(url, params=params, json=body, headers={**self._default_headers, **headers}) as result:
                result.raise_for_status()
                return await result.json()
