import os
from injector import singleton
from dotenv import load_dotenv

from models import Location

load_dotenv("dev.env")

@singleton
class Configuration:
    def __init__(self) -> None:
        self.app_name: str = os.environ["APP_NAME"]

        self.rabbit_connection: str = os.environ["RABBIT_CONNECTION"]

        self.redis_host: str = os.environ["REDIS_HOST"]
        self.redis_port: int = int(os.environ["REDIS_PORT"])
        
        self.influx_host: str = os.environ["INFLUX_HOST"]
        self.influx_port: int = int(os.environ["INFLUX_PORT"])
        self.influx_username: str = os.environ["INFLUX_USERNAME"]
        self.influx_password: str = os.environ["INFLUX_PASSWORD"]
        self.influx_db: str = os.environ["INFLUX_DB"]
        
        self.app_host: str = os.environ["APP_HOST"]
        self.app_port: int = int(os.environ["APP_PORT"])

        self.mongo_connection: str = os.environ["MONGO_CONNECTION"]

        self.open_weather_api_key = os.environ["OPEN_WEATHER_API_KEY"]
        self.locations = [Location(raw_location) for raw_location in os.environ["LOCATIONS"].split(';')]
        self.min_visibility = int(os.environ["MIN_VISIBILITY"])
        self.max_wind_speed = float(os.environ["MAX_WIND_SPEED"])
        self.job_interval_in_minutes = int(os.environ["JOB_INTERVAL_IN_MINUTES"])