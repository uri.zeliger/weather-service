import aioredis


class RedisClient:
    def __init__(self, redis_host: str, redis_port: int):
        self._redis_client = aioredis.Redis(host=redis_host, port=redis_port)

    async def set(self, key:str, value):
        await self._redis_client.set(key, value)

    async def get(self, key:str):
        return await self._redis_client.get(key)
