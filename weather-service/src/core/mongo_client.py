from injector import inject
from motor.motor_asyncio import AsyncIOMotorClient


class MongoClient:
    @inject
    def __init__(self, mongo_connection: str) -> None:
        self.mongo_client: AsyncIOMotorClient = AsyncIOMotorClient(
            mongo_connection)
        self.mongo_client
