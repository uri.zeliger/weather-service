from types import FunctionType
from injector import inject, singleton
import aio_pika


class RabbitConnection:
    def __init__(self, rabbit_connection):
        self._rabbit_connection = rabbit_connection

    async def connect(self):
        self._connection = await aio_pika.connect_robust(self._rabbit_connection)
        self._channel = await self._connection.channel()
        await self._channel.set_qos(prefetch_count=100)

    @property
    def connection(self):
        return self._connection

    @property
    def channel(self):
        return self._channel

    async def close(self):
        await self._channel.close()
        await self._connection.close()
