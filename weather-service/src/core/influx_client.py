from datetime import datetime
from aioinflux.client import InfluxDBClient


class InfluxClient:
    def __init__(self, host: str, port: int, username: str, password: str, db: str) -> None:
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.db = db

    async def connect(self):
        self._client = InfluxDBClient(
            host=self.host,
            port=self.port,
            username=self.username,
            password=self.password,
        )
        res = await self._client.show_databases()
        if self.db not in [item for sublist in res['results'][0]["series"][0]["values"] for item in sublist]:
            await self._client.create_database(self.db)

        self._client.db = self.db

    async def write_point(self, data: dict) -> bool:
        if self._client:
            return await self._client.write({
                'time': datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"),
                **data
            })
        
        return False

    async def read_points(self, query: str):
        if self._client:
            await self._client.query(query)

    def get_client(self):
        return self._client

    def __del__(self):
        self._client.close()
