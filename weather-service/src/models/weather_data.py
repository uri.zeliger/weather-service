from dataclasses import dataclass


@dataclass
class WeatherData:
    calculation_time: int
    visibility: int
    wind_speed: float