from dataclasses import dataclass


@dataclass
class Location:
    lat: str
    lon: str

    def __init__(self, init_str: str = None):
        if init_str:
            super()

        temp = init_str.split(",")
        self.lat = temp[0]
        self.lon = temp[1]

    def __str__(self):
        return f"{self.lat}.{self.lon}"
