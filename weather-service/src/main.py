from injector import Injector
from sanic_openapi import openapi2_blueprint
from sanic import Sanic

from core import IOC, Configuration, InfluxClient, JobManager
from api import register_routes, AlertSub
from tasks import WeatherSampleTask
from service_client import AlertPub, ServiceClientIOC

ioc_container = Injector(modules=[IOC, ServiceClientIOC])
config = ioc_container.get(Configuration)
influx_client = ioc_container.get(InfluxClient)
job_manager = ioc_container.get(JobManager)
weather_sample_task = ioc_container.get(WeatherSampleTask)

alert_sub = ioc_container.get(AlertSub)
alert_pub = ioc_container.get(AlertPub)

app = Sanic(config.app_name)
app.blueprint(openapi2_blueprint)


if __name__ == "__main__":
    register_routes(app, ioc_container)

    @app.listener("after_server_start")
    async def after_server_start(app, loop):
        job_manager.add_interval_job(
            weather_sample_task.run, config.job_interval_in_minutes)
        job_manager.start()
        await alert_sub.connect()
        await alert_sub.subscribe()
        await alert_pub.connect()
        await influx_client.connect()

    @app.listener("before_server_stop")
    async def before_server_stop(app, loop):
        job_manager.close()

    app.run(host=config.app_host, port=config.app_port)
