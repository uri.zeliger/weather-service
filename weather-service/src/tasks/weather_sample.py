from dataclasses import asdict
import logging
from typing import List

import asyncio
from injector import inject, singleton
from models import Location, WeatherData
from core import Configuration, InfluxClient
from dal import WeatherDAL
from service_client import AlertPub, WeatherServiceClient


@singleton
class WeatherSampleTask:
    @inject
    def __init__(self, config_service: Configuration,
                 weather_service_client: WeatherServiceClient,
                 logger: logging.Logger,
                 influx_dal: WeatherDAL,
                 alert_pub: AlertPub):
        self._logger = logger
        self._config_service = config_service
        self.__weather_dal = influx_dal
        self._weather_service_client = weather_service_client
        self._alert_pub = alert_pub

    async def run(self):
        background_tasks = set()
        for location in self._config_service.locations:
            task = asyncio.create_task(self._get_weather(location))

            background_tasks.add(task)

            task.add_done_callback(background_tasks.discard)

    async def _get_weather(self, location) -> None:
        weather_data: WeatherData = await self._weather_service_client.get_weather_async(location)
        data = {'location': str(location), **asdict(weather_data)}
        await self.__weather_dal.insert(data)
        await self._alert_pub.publish_async(data)
