from logging import Logger
import aio_pika
from injector import inject, singleton
from core import RabbitConnection, umsgpack_serializer
from bl import WeatherAlertCommand


@singleton
class AlertSub:
    @inject
    def __init__(self, logger: Logger, rabbit_connection: RabbitConnection, command: WeatherAlertCommand):
        self._logger = logger
        self._rabbit_connection = rabbit_connection
        self._command = command
        self._exchange_name = 'weather'
        self._queue_name = 'alerts'

    async def connect(self):
        await self._rabbit_connection.connect()

    async def subscribe(self):
        channel = self._rabbit_connection.channel

        self._exchange = await self._rabbit_connection.channel.declare_exchange(self._exchange_name, aio_pika.ExchangeType.FANOUT)

        queue = await channel.declare_queue(self._queue_name)

        await queue.bind(self._exchange)

        await queue.consume(self.execute)

    async def execute(self, message: aio_pika.IncomingMessage):
        async with message.process():
            try:
                msg = umsgpack_serializer.unpackb(message.body)
                await self._command.execute(msg)
            except Exception:
                self._logger.error(
                    "Failed to process weather data notification", exc_info=True)
