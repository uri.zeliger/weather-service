from sanic import Sanic, Blueprint, Request, HTTPResponse, text
from injector import Injector, inject, singleton

@singleton
class SnapshotController:
    @inject
    def __init__(self):
        pass

    async def get(self, request: Request, key: str) -> HTTPResponse:
        return text("ok")

    async def post(self, request: Request) -> HTTPResponse:
        return text("ok", status=201)

def register_snapshot(self: SnapshotController) -> Blueprint:
    bp_snapshot = Blueprint('snapshot', url_prefix='/snapshot')

    @bp_snapshot.post("/")
    async def post_snapshot(request: Request) -> HTTPResponse:
        return await self.post(request)

    @bp_snapshot.get("/<key:str>")
    async def get_snapshot(request: Request, key: str) -> HTTPResponse:
        return await self.get(request, key)

    return bp_snapshot
