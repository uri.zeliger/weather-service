from dataclasses import asdict
from sanic import Blueprint, Request, HTTPResponse
from sanic import response
from sanic.exceptions import ServerError
from injector import inject, singleton
from models import Location
from service_client import WeatherServiceClient


@singleton
class WeatherDataController:
    @inject
    def __init__(self, weather_service_client: WeatherServiceClient):
        self._weather_service_client = weather_service_client

    async def get(self, request: Request) -> HTTPResponse:
        try:
            lat = request.args.get("lat")
            lon = request.args.get("lon")
            location = Location(lat, lon)
            res = await self._weather_service_client.get_weather_async(location)
            return response.json(asdict(res), status=200)
        except Exception as e:
            print(str(e))
            return ServerError('unexpected error')

def register_weather_data(self: WeatherDataController)-> Blueprint:
    weather_snapshot = Blueprint('weather_data', url_prefix='/weather_data')

    @weather_snapshot.get("/")
    def get_weather(request: Request) -> HTTPResponse:
        return self.get(request)

    return weather_snapshot