from sanic import Request, HTTPResponse, text
from sanic.views import HTTPMethodView
from injector import inject, singleton

@singleton
class HealthCheckController(HTTPMethodView):
    @inject
    def __init__(self):
        super()

    def get(self, request: Request) -> HTTPResponse:
        return text("ok")
