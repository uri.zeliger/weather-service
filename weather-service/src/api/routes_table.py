from sys import prefix
from urllib.request import Request
from sanic import Sanic, Blueprint, Request, HTTPResponse
from injector import Injector
from .health_check_controller import HealthCheckController
from .weather_data_controller import WeatherDataController, register_weather_data 
from .snapshot_controller import SnapshotController, register_snapshot 


def register_routes(app: Sanic, ioc: Injector) -> None:
    health_check_controller = ioc.get(HealthCheckController)
    app.blueprint(register_snapshot(ioc.get(SnapshotController)))

    app.blueprint(register_weather_data(ioc.get(WeatherDataController)))

    app.add_route(health_check_controller.as_view(), "/health_check")
