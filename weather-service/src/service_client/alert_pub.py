import aio_pika
from injector import inject, singleton
from core import RabbitConnection, umsgpack_serializer


@singleton
class AlertPub:
    @inject
    def __init__(self, rabbit_connection: RabbitConnection):
        self._rabbit_connection = rabbit_connection
        self._exchange_name = 'weather'
        self._queue_name = 'alerts'

    async def connect(self):
        await self._rabbit_connection.connect()
        self._exchange = await self._rabbit_connection.channel.declare_exchange(self._exchange_name, aio_pika.ExchangeType.FANOUT)


    async def reconnect(self):
        await self._rabbit_connection.connection.close()
        await self._rabbit_connection.connection.connect()
        
   
    async def publish_async(self, msg):
        """
        Publishes a message to the exchange. Reconnects on errors of type `ConnectionError`.

        Args:
            msg (_type_): _description_
        """

        serialized_msg = umsgpack_serializer.packb(msg)

        amqp_msg = aio_pika.Message(
            serialized_msg,
            delivery_mode=aio_pika.DeliveryMode.PERSISTENT
        )

        try:
            # ! routing_key is ignored for fanout exchanges
            await self._exchange.publish(amqp_msg, routing_key=self._queue_name)
        except ConnectionError as ex:
            await self.reconnect()
            await self._exchange.publish(amqp_msg, routing_key=self._queue_name)
