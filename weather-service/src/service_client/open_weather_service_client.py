from dataclasses import asdict
from injector import inject, singleton

from .weather_service_client import WeatherServiceClient
from core import RestExecuter
from models import Location, WeatherData

@singleton
class OpenWeatherServiceClient(WeatherServiceClient):
    @inject
    def __init__(self, rest_executer: RestExecuter, api_key) -> None:
        self._rest_executer = rest_executer
        self._params = {"appid": api_key}

    def get_weather(self, location: Location):
        try:
            params = {**self._params, **asdict(location)}
            result = self._rest_executer.get(
                f"https://api.openweathermap.org/data/2.5/weather", params=params)

            # TODO map response
        except Exception as e:
            print(str(e))
        return result

    async def get_weather_async(self, location: Location):
        try:
            params = {**self._params, **asdict(location)}
            result = await self._rest_executer.get_async(
                f"https://api.openweathermap.org/data/2.5/weather", params=params)

            return  WeatherData(visibility=result["visibility"], wind_speed=result["wind"]["speed"], calculation_time=result["dt"])
        except Exception as e:
            print(str(e))
        return result
