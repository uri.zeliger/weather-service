from dataclasses import asdict
from injector import inject
from core import RestExecuter
from models import Location, WeatherData


class WeatherServiceClient:
    """This class is an interface for getting weather information in align stucture"""

    def __init__(self) -> None:
        pass

    def get_weather(self, location: Location) -> WeatherData:
        raise NotImplemented('You mast implement this method')

    async def get_weather_async(self, location: Location) -> WeatherData:
        raise NotImplemented('You mast implement this method')
