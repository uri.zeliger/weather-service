import logging
from injector import Module, provider, singleton
from sanic import Sanic

from .open_weather_service_client import OpenWeatherServiceClient
from .weather_service_client import WeatherServiceClient
from core import Configuration, RestExecuter

class ServiceClientIOC(Module):
    @provider
    def provide_weather_service_client(self, config_serive: Configuration, rest_executer: RestExecuter) -> WeatherServiceClient:
        return OpenWeatherServiceClient(rest_executer, config_serive.open_weather_api_key)
