from typing import Any
from injector import inject, singleton
from core import Configuration
from dal import AlertDAL


@singleton
class WeatherAlertCommand:
    @inject
    def __init__(self, config_service: Configuration, alert_dal: AlertDAL):
        self._config_service = config_service
        self._alert_dal = alert_dal
        self._exchange_name = 'weather'
        self._queue_name = 'alerts'

    async def execute(self, msg:Any):
        if msg["visibility"] > self._config_service.min_visibility and \
                msg["wind_speed"] < self._config_service.max_wind_speed:
            return

        alert_id = msg["location"]
        del msg["location"]
        alert = await self._alert_dal.get(alert_id)
        if not alert:
            alert = await self._alert_dal.insert(alert_id)

        await self._alert_dal.update(alert, alert_id, msg)
            
